'Access module to import all excel sheets exists in a folder as tables via "DoCmd.TransferSpreadsheet" method within Access API
'https://docs.microsoft.com/en-us/office/vba/api/access.docmd.transferspreadsheet
'Access don't accept table name lenght more than 64. So there is a workaround for this in the code

'Fatih Mehmet Özcan
'2019.01
'Use at your own risk


'Attribute VB_Name = "gitlab_moduleAccessImportExcel"
Option Explicit
Private Const location as string = "your folder location"

'variables for file and sheet count
Dim counterMain, counterSub, counterTotal, tempTrigger As Integer

'variable to list founded files to a notepad
Dim listFolderFiles, tempTextSub, tempTextMain, tempTextTotal As String

Dim XL As Object
Dim wk As Workbook


Private Sub mainFor()

'variable to get folders and files
Dim fso As Object
Dim fol As Folder
Set fso = CreateObject("Scripting.FileSystemObject")
Set fol = fso.GetFolder(location)
Dim fil As File
Set XL = CreateObject("Excel.Application")

'tempTextMain is used as file name
tempTextMain = Empty
'tempTextSub is used as sheet name
tempTextSub = Empty
'listFolderFiles is used to get all stored string and export to notepad
listFolderFiles = Empty
'counterSub count sheets in a file
counterSub = 0
'counterMain counts files in specified path
counterMain = 1
'counterTotal counts all
counterTotal = 0

tempTextTotal = Empty
tempTrigger = 0

'call for function
LoopFor fol


End Sub


'this function is called from LoopFor function
Private Function importFromExcel(wkBookPath As String)

Dim ws As Worksheet
Dim i, minus As Integer
Dim tableName, sheetName, tableNameCrop, tempTableName As String
minus = 0
i = 0

'i had to restate this statement because i had problems reaching the other one in main sub
Set XL = CreateObject("Excel.Application")

XL.ScreenUpdating = False
XL.Visible = False
XL.DisplayAlerts = False
XL.EnableEvents = False


'Error handling if openning files gives an error.
On Error GoTo HataDevam

'open an excel file
Set wk = XL.Workbooks.Open(wkBookPath, 0, UpdateLinks:=False, ReadOnly:=True)

        counterMain = counterMain + 1
        
'loop for all sheets in a opened file
For Each ws In wk.Worksheets

'there are some statements regarding used file and sheet names in current specific conditions
'if $ character exists in sheet name, remove it
If InStr(ws.Name, "$") Then
sheetName = Replace(ws.Name, "$", "")
Else
sheetName = ws.Name
End If

counterTotal = counterTotal + 1
tempTrigger = 1

        If counterTotal < 10 Then
        tempTextTotal = "000" & counterTotal
        ElseIf counterTotal > 9 And counterTotal < 100 Then
        tempTextTotal = "00" & counterTotal
        ElseIf counterTotal > 99 And counterTotal < 1000 Then
        tempTextTotal = "0" & counterTotal
        Else
        tempTextTotal = counterTotal
        End If

counterSub = counterSub + 1


'if numbers at the start of the name goes like -7-8-9-10. 10 takes place at first. i don't know if this is a vba or office issue
'so i just put a zero before single character number
If counterSub < 10 Then
tempTextSub = "0" & counterSub
Else
tempTextSub = counterSub
End If

'name set up
'name is shortened to 64 if exceeds*work Sheet Name
'in this step, .xlsx extension in file is not replaced. check a few lines below for replacement step
tableName = tempTextTotal & "*" & tempTextMain & "*" & tempTextSub & "*" & wk.Name & "*" & sheetName


'check if file name contains "xlsx"
If InStr(LCase(tableName), "xlsx") Then

'process to shorten the name to be used for access table. tables can not be longer than 64
If Len(Replace(LCase(tableName), ".xlsx", "")) > 64 Then
minus = Len(Replace(LCase(tableName), ".xlsx", "")) - 63
tempTableName = Left(Replace(LCase(wk.Name), ".xlsx", ""), Len(Replace(LCase(wk.Name), ".xlsx", "")) - minus)
tableNameCrop = tempTextTotal & "*" & tempTextMain & "*" & tempTextSub & "*" & tempTableName & "*" & sheetName
'Debug.Print tableNameCrop & " - " & Len(tableNameCrop)
'listFolderFiles = listFolderFiles + tableNameCrop & " - " & Len(tableNameCrop) & vbCrLf
Else
tableNameCrop = Replace(LCase(tableName), ".xlsx", "")
'Debug.Print tableNameCrop & " - " & Len(tableNameCrop)
'listFolderFiles = listFolderFiles + tableNameCrop & " - " & Len(tableNameCrop) & vbCrLf
End If

Else

'same thing if file extension is only .xls
If Len(Replace(LCase(tableName), ".xls", "")) > 64 Then
minus = Len(Replace(LCase(tableName), ".xls", "")) - 63
tempTableName = Left(Replace(LCase(wk.Name), ".xls", ""), Len(Replace(LCase(wk.Name), ".xls", "")) - minus)
tableNameCrop = tempTextTotal & "*" & tempTextMain & "*" & tempTextSub & "*" & tempTableName & "*" & sheetName
'Debug.Print tableNameCrop & " - " & Len(tableNameCrop)
'listFolderFiles = listFolderFiles + tableNameCrop & " - " & Len(tableNameCrop) & vbCrLf
Else
tableNameCrop = Replace(LCase(tableName), ".xls", "")
'Debug.Print tableNameCrop & " - " & Len(tableNameCrop)
'listFolderFiles = listFolderFiles + tableNameCrop & " - " & Len(tableNameCrop) & vbCrLf
End If

End If



'import opened excel file sheet as an access table
On Error Resume Next
DoCmd.TransferSpreadsheet acImport, acSpreadsheetTypeExcel12, tableNameCrop, wkBookPath, False, sheetName & "!"

'Debug.Print tableNameCrop


'excel sheets loop
Next


'close if no other sheet
wk.Close (False)
Set XL = Nothing
Set wk = Nothing
counterSub = 0
tempTextSub = Empty
Exit Function

'Error handling
HataDevam:
counterSub = 0
tempTextSub = Empty


If tempTrigger = 0 Then
counterTotal = counterTotal + 1
        If counterTotal < 10 Then
        tempTextTotal = "000" & counterTotal
        ElseIf counterTotal > 9 And counterTotal < 100 Then
        tempTextTotal = "00" & counterTotal
        ElseIf counterTotal > 99 Then
        tempTextTotal = "0" & counterTotal
        Else
        tempTextTotal = counterTotal
        End If
        tempTrigger = 0
End If


XL.ScreenUpdating = True
XL.Visible = True
XL.DisplayAlerts = True
XL.EnableEvents = True
Set XL = Nothing
Set wk = Nothing

End Function



'basit loop function for folders and subfolders' files
Private Function LoopFor(klasor As Folder)

Dim subFol As Folder
Dim subFil As File

For Each subFil In klasor.Files

    'if file name contains .xls extension, also works for .xlsx
    'some files are temporary created then excel is working. check if not temporary
    If InStr(LCase(subFil), ".xls") > 0 And InStr(Left(subFil, 2), "$") = 0 Then

        'add zero for lower numbers than 10
        'add two zeros for lower numbers than 100
        'else make equal to current value. there should be a control for files numbers larger than 1000, but we don't have that much file
        If counterMain < 10 Then
        tempTextMain = "00" & counterMain
        ElseIf counterMain > 9 And counterMain < 100 Then
        tempTextMain = "0" & counterMain
        Else
        tempTextMain = counterMain
        End If
        
        'call for function with excel file path
        importFromExcel subFil.Path
    End If
Next subFil

'loop within subfolders
For Each subFol In klasor.SubFolders
LoopFor subFol
Next subFol

End Function